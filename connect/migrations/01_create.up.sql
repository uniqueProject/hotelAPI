create table users (
                       id UUID not null primary key,
                       first_name varchar(20) not null,
                       last_name varchar(20) not null,
                       user_name varchar(20) not null unique,
                       password varchar(100) not null,
                       email varchar(20) not null unique,
                       phone varchar(11) not null unique,
                       budget int not null
);

create table owners (
                        id UUID not null primary key,
                        first_name varchar(20) not null,
                        last_name varchar(20) not null,
                        user_name varchar(20) not null unique,
                        password varchar(110) not null,
                        email varchar(20) not null unique,
                        phone varchar(11) not null unique
);

create table admins (
                        id UUID not null primary key,
                        first_name varchar(20) not null,
                        last_name varchar(20) not null,
                        user_name varchar(20) not null unique,
                        password varchar(110) not null,
                        email varchar(20) not null unique,
                        phone varchar(11) not null unique
);
create table hotels (
                        id UUID not null primary key,
                        name VARCHAR(20) not null,
                        star int not null,
                        owner_id UUID not null references owners(Id),
                        admin_id UUID not null references admins(Id),
                        address varchar(200) not null unique,
                        history text not null,
                        budget int not null
);

create table rooms (
                       id UUID not null primary key,
                       price int not null,
                       hotel_id UUID not null references hotels(Id),
                       room_number int not null,
                       type int not null,
                       user_id uuid references users(Id),
                       status boolean not null,
                       persons int not null,
                       expire timestamp not null default current_timestamp
);