insert into users values (
                             '261f89bc-b0ef-40dc-86a3-0e036a420e2b',
                             'Ali',
                             'Bektemirov',
                             'ali007',
                             '$2a$05$FnQCVcbY7ryzaq83v5w8vONi0lC0LhzAeLeHtCY92zhXvE6OWmHqC',
                             'ali007@gmail.com',
                             '997771654',
                             2000
                             );
insert into users values (
                             '50a0e1ed-d32a-4f4a-907d-eb048eaad56b',
                             'John',
                             'Hang',
                             'johnhang',
                             '$2a$05$DqsJWCRqHid9C04BzRx0KeLxkkUCiix.pPI2jj7RMlW9mNaao8N7i',
                             'johnhang@gmail.com',
                             '997058554',
                             1200
                             );

insert into owners values (
                              '854901d5-e9a9-4d30-af91-7c474af105d4',
                              'Margana',
                              'Jasurova',
                              'marganaMe',
                              '$2a$05$Mb//aX.pEs2cEPfgQrXgUeFXURXDfjamzfYWyfNbEg9KVAFUSSss2',
                              'marganame@gmail.com',
                              '908784593'
                              );

insert into admins values (
                              'e9adf67e-2e93-41ae-9c3a-6e161b1ed4c9',
                              'Artur',
                              'Pentragon',
                              'arturSelfish',
                              '$2a$05$.hG/f/.dZGGBaJ9q9kPPNehHukC6c34.V3TwD/T0mKWm4ka51xmGu',
                              'arturSelf3@gmail.com',
                              '908784593'
                              );

insert into hotels values (
                              'd2a0eebf-4189-492d-aefe-db2e6add1c39',
                              'Kamilot',
                              4,
                              '854901d5-e9a9-4d30-af91-7c474af105d4',
                              'e9adf67e-2e93-41ae-9c3a-6e161b1ed4c9',
                              'Chilonzor tumani, Rahon restarani qarshisida',
                              ' ',
                              0
                              );

insert into rooms values (
                             '8f321bf4-2ec7-4644-98ae-b650d67ca89f',
                             135,
                             'd2a0eebf-4189-492d-aefe-db2e6add1c39',
                             101,
                             1,
                             null,
                             true,
                             2
                             );
insert into rooms values (
                             'd1449419-717c-4c1f-bd08-4bb8832dfefa',
                             115,
                             'd2a0eebf-4189-492d-aefe-db2e6add1c39',
                             103,
                             2,
                             null,
                             true,
                             1
                             );
insert into rooms values (
                             '6acb436f-1c21-4a84-9f72-4a29c762c58a',
                             75,
                             'd2a0eebf-4189-492d-aefe-db2e6add1c39',
                             202,
                             3,
                             null,
                             true,
                             4
                             );
insert into rooms values (
                             '6510e8d1-a299-4af1-8620-92fbee6309bb',
                             145,
                             'd2a0eebf-4189-492d-aefe-db2e6add1c39',
                             105,
                             1,
                             null,
                             true,
                             1
                             );