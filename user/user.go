package user

import (
	"fmt"
	"hotel/data"
	"os"
	"os/exec"
	"sort"
	"text/tabwriter"
	"time"
)

var (
	Reset  = "\033[0m"
	Red    = "\033[31m"
	Green  = "\033[32m"
	Yellow = "\033[33m"
)

type User data.User

func (u User) NewWrite() {
	all := data.ReadFromAll()
	user := data.User{
		Id:       u.Id,
		Name:     u.Name,
		Lastname: u.Lastname,
		UserName: u.UserName,
		Password: u.Password,
		Email:    u.Email,
		Phone:    u.Phone,
		Budget:   u.Budget,
		HotelIds: u.HotelIds,
	}
	all.Users = append(all.Users, user)
	data.WriteToAll(all)
}

func (u User) List() {
	listOfHotels := data.ReadFromHotel()
	w := tabwriter.NewWriter(os.Stdout, 1, 4, 1, '\t', tabwriter.AlignRight)
	clear()
	fmt.Printf("№\tName\t\t\tRate\t\tAddress\n\n")
	for i, val := range listOfHotels {
		star := ""
		for i := uint8(0); i < val.Star; i++ {
			star += "* "
		}
		fmt.Fprintf(w, "%d\t%s%s\t\t%s%s\t\t%s%s\n", i+1, Red, val.Name, Yellow, star, Reset, val.Address)
	}
	w.Flush()
}

func (u User) Rooms(indexOfHotel int) bool {
	persons := make([][]data.Room, 12)
	listOfHotels := data.ReadFromHotel()
	for i, hotel := range listOfHotels {
		if i == indexOfHotel-1 {
			clear()
			for _, room := range hotel.Rooms {
				if room.Type == 1 {
					persons[0+room.Persons-1] = append(persons[0+room.Persons-1], room)
				} else if room.Type == 2 {
					persons[4+room.Persons-1] = append(persons[4+room.Persons-1], room)
				} else if room.Type == 3 {
					persons[8+room.Persons-1] = append(persons[8+room.Persons-1], room)
				}
			}
			for y := range persons {
				sort.Slice(persons[y], func(a, b int) bool {
					return persons[y][b].NumberRoom > persons[y][a].NumberRoom
				})
			}
			printRooms(persons)
			return true
		}
	}
	return false
}

func printRooms(persons [][]data.Room) {
	for y := range persons {
		if y < 4 {
			if len(persons[y]) != 0 {
				fmt.Printf("Lux room to %d person(s) costs %d\n", y+1, persons[y][0].Price)
			}
			for i, x := range persons[y] {
				if i%10 == 0 {
					fmt.Println()
				}
				if x.Status {
					fmt.Printf("| %s%d%s | ", Green, x.NumberRoom, Reset)
				} else {
					fmt.Printf("| %s%d%s | ", Red, x.NumberRoom, Reset)
				}
			}
		} else if y < 8 && y > 3 {
			if len(persons[y]) != 0 {
				fmt.Printf("\nPremium room to %d person(s) costs %d\n", y-3, persons[y][0].Price)
			}
			for i, x := range persons[y] {
				if i%10 == 0 {
					fmt.Println()
				}
				if x.Status {
					fmt.Printf("| %s%d%s | ", Green, x.NumberRoom, Reset)
				} else {
					fmt.Printf("| %s%d%s | ", Red, x.NumberRoom, Reset)
				}
			}
		} else if y < 12 && y > 7 {
			if len(persons[y]) != 0 {
				fmt.Printf("\nOrdinary room to %d person(s) costs %d\n", y-7, persons[y][0].Price)
			}
			for i, x := range persons[y] {
				if i%10 == 0 {
					fmt.Println()
				}
				if x.Status {
					fmt.Printf("| %s%d%s | ", Green, x.NumberRoom, Reset)
				} else {
					fmt.Printf("| %s%d%s | ", Red, x.NumberRoom, Reset)
				}
			}
		}
	}
	fmt.Println()
}

func (u User) UserProfile() {
	clear()
	fmt.Println("Name: ", u.Name)
	fmt.Println("Lastname: ", u.Lastname)
	fmt.Println("Username: ", u.UserName)
	fmt.Print("Password: ")
	fmt.Print("* * * * * *")
	fmt.Println()
	fmt.Println("Budget: ", u.Budget)
	fmt.Println("Phone: ", u.Phone)
	fmt.Println("Email: ", u.Email)
}

func (u *User) Booking(numOfRoom uint16, index int, day int) (bool, bool) {
	listOfHotels := data.ReadFromHotel()
	for i, hotel := range listOfHotels {
		if index-1 == i {
			for in, room := range hotel.Rooms {
				if room.NumberRoom == numOfRoom {
					if !(room.Status) {
						return false, true
					}
					if u.Budget > uint32(room.Price)*uint32(day) {
						u.Budget -= uint32(room.Price) * uint32(day)
						listOfHotels[i].Rooms[in].Status = false
						listOfHotels[i].Rooms[in].UserId = u.Id
						u.HotelIds = append(u.HotelIds, hotel.Id)
						listOfHotels[i].Budget += uint(room.Price)
						listOfHotels[i].Rooms[in].Expire = time.Now().Add(time.Hour * 24 * time.Duration(day))
						y, m, d := time.Now().Date()
						h, min, _ := time.Now().Clock()
						listOfHotels[i].History += fmt.Sprintf("%d %s %d %d:%d: %s %s ordered the room №%d for %d day(s)\n", y, m, d, h, min, u.Name, u.Lastname, room.NumberRoom, day)
						data.WriteToHotel(listOfHotels)
						u.updateUser()
						return true, true
					} else {
						fmt.Println("You dont have enough money!")
						return false, false
					}
				}
			}
		}
	}
	return true, false
}

func (u *User) ShowBooking() {
	ls := make(map[string][]uint16)
	hotels := data.ReadFromHotel()
	users := data.ReadFromAll().Users
	for _, user := range users {
		if user.Id == u.Id {
			u.HotelIds = user.HotelIds
		}
	}
	for _, hotelId := range u.HotelIds {
		for _, hotel := range hotels {
			if hotel.Id == hotelId {
				l := make([]uint16, 0)
				for _, room := range hotel.Rooms {
					if room.UserId == u.Id {
						l = append(l, room.NumberRoom)
					}
				}
				ls[hotel.Name] = l
			}
		}
	}
	clear()
	for name, nums := range ls {
		fmt.Print(name, ": ")
		for _, num := range nums {
			fmt.Print("| ", num, " | ")
		}
		fmt.Println()
	}
}

func (u User) ChangePassword(password string) {
	all := data.ReadFromAll()
	for i, user := range all.Users {
		if user.Id == u.Id {
			all.Users[i].Password = password
			clear()
			fmt.Println("You have changed your Password successfully")
		}
	}
	data.WriteToAll(all)
}

func (u User) ChangeUsername(username string) {
	all := data.ReadFromAll()
	for i, user := range all.Users {
		if user.Id == u.Id {
			all.Users[i].UserName = username
			clear()
			fmt.Println("You have changed your Username successfully")
		}
	}
	data.WriteToAll(all)
}

func (u User) updateUser() {
	all := data.ReadFromAll()
	for i, x := range all.Users {
		if x.Id == u.Id {
			all.Users[i].Budget = u.Budget
			all.Users[i].HotelIds = u.HotelIds
		}
	}
	data.WriteToAll(all)
}

func clear() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()
}
