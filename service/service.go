package service

import (
	"hotel/repository"
)

type Service struct {
	soburjonRepo   SoburjonRepo
	pulatRepo      PulatRepo
	abdurahmonRepo AbdurahmonRepo
}

func New(repo repository.PsqlRepo) *Service {
	return &Service{
		soburjonRepo: repo,
		pulatRepo:    repo,
	}
}
