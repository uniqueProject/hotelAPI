package service

import (
	"context"
	"github.com/google/uuid"
	"hotel/repository"
	"testing"
	"time"
)

type SpyPulatRepo struct{}

func (s *SpyPulatRepo) RoomsListOfHotel(ctx context.Context, hotelId string) ([]repository.RoomShow, error) {
	rooms := []repository.RoomShow{
		{
			Id:         uuid.NewString(),
			Price:      85,
			HotelId:    uuid.NewString(),
			NumberRoom: 103,
			Type:       2,
			Status:     false,
			Persons:    3,
		},
		{
			Id:         uuid.NewString(),
			Price:      95,
			HotelId:    uuid.NewString(),
			NumberRoom: 102,
			Type:       2,
			Status:     false,
			Persons:    4,
		},
	}
	return rooms, nil
}

func (s *SpyPulatRepo) RoomBooking(ctx context.Context, userId, roomId string) error {
	return nil
}

func (s *SpyPulatRepo) GetRoom(ctx context.Context, roomId string) (repository.RoomShow, error) {
	return repository.RoomShow{}, nil
}

func (s *SpyPulatRepo) GetUser(ctx context.Context, userId string) (repository.User, error) {
	return repository.User{}, nil
}

func (s *SpyPulatRepo) UpdateUsersBudget(ctx context.Context, userId string, sum uint16) error {
	return nil
}

func (s *SpyPulatRepo) UpdateHotelsBudget(ctx context.Context, hotelId string, sum uint16) error {
	return nil
}

func (s *SpyPulatRepo) UpdateRoom(ctx context.Context, userId, roomId string, t time.Duration) error {
	return nil
}

func (s *SpyPulatRepo) OrderedRooms(ctx context.Context, userId string) ([]repository.RoomShow, error) {
	return nil, nil
}

func (s *SpyPulatRepo) UpdateUser(ctx context.Context, userId string, newUser repository.User) error {
	return nil
}

/*
	user := repository.User{
		Id:        uuid.NewString(),
		FirstName: "Pulat",
		Lastname:  "Nazarov",
		UserName:  "pulatnazarov",
		Password:  "Pulat123",
		Email:     "pulat@gmail.com",
		Phone:     "907138899",
		Budget:    200,
	}
*/
func (s *SpyPulatRepo) RegisterUser(ctx context.Context, user repository.User) error {
	return nil
}

func (s *SpyPulatRepo) HotelsList(ctx context.Context) ([]repository.HotelShow, error) {
	hotels := []repository.HotelShow{
		{
			Id:      uuid.NewString(),
			Name:    "Grand",
			Star:    3,
			AdminId: uuid.NewString(),
			Address: "Chilanzar 9 kv",
		},
		{
			Id:      uuid.NewString(),
			Name:    "Hilton",
			Star:    4,
			AdminId: uuid.NewString(),
			Address: "Shayhantahur",
		},
	}
	return hotels, nil
}

func TestService_ListHotels(t *testing.T) {
	sr := SpyPulatRepo{}
	got, er := Service{pulatRepo: &sr}.ListHotels(context.Background())
	if er != nil {
		t.Error("error1: ", er)
	}
	hotels, er := sr.HotelsList(context.Background())
	if er != nil {
		t.Error("error1.5: ", er)
	}
	if len(hotels) != len(got) {
		t.Error("error2: ", er)
	}
}

func TestService_UserRegister(t *testing.T) {
	sr := SpyPulatRepo{}
	user := repository.User{}
	got, er := Service{pulatRepo: &sr}.UserRegister(context.Background(), user)
	if er != nil {
		t.Error("error3: ", er)
	}

	if _, er := uuid.Parse(got); er != nil {
		t.Error("error4: ", er)
	}
}

func TestService_UserUpdate(t *testing.T) {
	sr := SpyPulatRepo{}
	userId := uuid.NewString()
	newUser := repository.User{}
	newUser.Password = "Pulat1234"
	er := Service{pulatRepo: &sr}.UserUpdate(context.Background(), userId, newUser)
	if er != nil {
		t.Error("error5: ", er)
	}
}

func TestService_ListRoomsOfHotel(t *testing.T) {
	sr := SpyPulatRepo{}
	hotelId := uuid.NewString()
	room, er := Service{pulatRepo: &sr}.ListRoomsOfHotel(context.Background(), hotelId)
	if er != nil {
		t.Error("error6: ", er)
	}
	rooms, er := sr.RoomsListOfHotel(context.Background(), hotelId)
	if er != nil {
		t.Error("error6.5", er)
	}
	if len(room) != len(rooms) {
		t.Error("error7: ", er)
	}
}

func TestService_BookingRoom(t *testing.T) {
	sr := SpyPulatRepo{}
	hotelId := uuid.NewString()
	userId := uuid.NewString()
	roomId := uuid.NewString()
	days := 7
	er := Service{pulatRepo: &sr}.BookingRoom(context.Background(), hotelId, userId, roomId, days)
	if er != nil {
		t.Error("error8: ", er)
	}
}

func TestService_MyOrders(t *testing.T) {
	sr := SpyPulatRepo{}
	userId := uuid.NewString()
	rooms, er := Service{pulatRepo: &sr}.MyOrders(context.Background(), userId)
	if er != nil {
		t.Error("error9: ", er)
	}
	if len(rooms) != 0 {
		t.Error("error10 not enough rooms")
	}
}
