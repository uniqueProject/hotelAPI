package service

import (
	"context"
	"database/sql"
	"errors"
	"github.com/google/uuid"
	"hotel/repository"
	"testing"
)

type SpyRepo struct {
	s int
	n int
}

var newerror = errors.New("err")

func (r *SpyRepo) InsertHotel(ctx context.Context, hotel repository.Hotel) error {
	_, err := uuid.Parse(hotel.Id)
	return err
}
func (r *SpyRepo) InsertRoom(ctx context.Context, room repository.Room) error {
	numbers := []uint16{100, 101, 102, 103, 104, 105, 109, 115, 116, 117, 200, 201, 202, 203, 204, 205, 209, 215, 216, 217}
	r.n++
	for _, n := range numbers {
		if room.NumberRoom == n {
			r.s++
		}
	}
	if r.s != r.n {
		return errors.New("error number")
	}
	return nil
}
func (r *SpyRepo) LogInUser(ctx context.Context, username, password string) (string, string, error) {
	if r.s == 1 {
		return "uuid", "$2a$05$FnQCVcbY7ryzaq83v5w8vONi0lC0LhzAeLeHtCY92zhXvE6OWmHqC", nil
	}
	return "", "", sql.ErrNoRows
}
func (r *SpyRepo) LogInOwner(ctx context.Context, username, password string) (string, string, error) {
	if r.s == 1 {
		return "uuid", "$2a$05$Mb//aX.pEs2cEPfgQrXgUeFXURXDfjamzfYWyfNbEg9KVAFUSSss2", nil
	}
	return "", "", sql.ErrNoRows
}
func (r *SpyRepo) LogInAdmin(ctx context.Context, username, password string) (string, string, error) {
	switch r.s {
	case 1:
		return "uuid", "$2a$05$.hG/f/.dZGGBaJ9q9kPPNehHukC6c34.V3TwD/T0mKWm4ka51xmGu", nil
	case 2:
		return "", "", newerror
	}
	return "", "", sql.ErrNoRows
}
func TestHotelRegister(t *testing.T) {
	sr := SpyRepo{}
	hotel := repository.Hotel{}
	got, err := Service{soburjonRepo: &sr}.HotelRegister(context.Background(), hotel)
	if err != nil {
		t.Error(err)
	}
	_, err = uuid.Parse(got)
	if err != nil {
		t.Error(err)
	}
}
func TestAddRoom(t *testing.T) {
	sr := SpyRepo{}
	room := repository.Room{}
	number := "100- 105, 109,115 -117,200-205,209,215 - 217"
	err := Service{soburjonRepo: &sr}.AddRoom(context.Background(), room, number)
	if err != nil {
		t.Error(err)
	}
	if sr.s != 20 {
		t.Error("error numbers")
	}
}

func TestLogIn(t *testing.T) {
	t.Run("test 1", func(t *testing.T) {
		sr := SpyRepo{s: 1}
		id, rol, err := Service{soburjonRepo: &sr}.LogIn(context.Background(), "name", "Alibek007")
		if err != nil {
			t.Error(err)
		}
		if rol != 1 {
			t.Error("rol error")
		}
		if id != "uuid" {
			t.Error("id error")
		}
	})
	t.Run("test 2", func(t *testing.T) {
		sr := SpyRepo{s: 1}
		_, _, err := Service{soburjonRepo: &sr}.LogIn(context.Background(), "name", "libek007")
		if !errors.Is(err, PasswordError) {
			t.Error("password check error")
		}
	})
	t.Run("test 3", func(t *testing.T) {
		sr := SpyRepo{s: 2}
		_, _, err := Service{soburjonRepo: &sr}.LogIn(context.Background(), "name", "Alibek007")
		if !errors.Is(err, newerror) {
			t.Error("return error error")
		}

	})
	t.Run("test 4", func(t *testing.T) {
		sr := SpyRepo{s: 3}
		_, _, err := Service{soburjonRepo: &sr}.LogIn(context.Background(), "name", "Alibek007")
		if !errors.Is(err, NoUser) {
			t.Error("return error error")
		}

	})
}
