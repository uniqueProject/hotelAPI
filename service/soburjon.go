package service

import (
	"context"
	"database/sql"
	"errors"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"hotel/repository"
	"strconv"
	"strings"
)

var (
	PasswordError = errors.New("password error")
	NoUser        = errors.New("username not found")
)

type SoburjonRepo interface {
	InsertHotel(ctx context.Context, hotel repository.Hotel) error
	InsertRoom(ctx context.Context, hotel repository.Room) error
	LogInUser(ctx context.Context, username, password string) (string, string, error)
	LogInOwner(ctx context.Context, username, password string) (string, string, error)
	LogInAdmin(ctx context.Context, username, password string) (string, string, error)
}

func (p Service) HotelRegister(ctx context.Context, hotel repository.Hotel) (string, error) {
	hotel.Id = uuid.NewString()
	if err := p.soburjonRepo.InsertHotel(ctx, hotel); err != nil {
		return "", err
	}

	return hotel.Id, nil
}

func (p Service) AddRoom(ctx context.Context, room repository.Room, numbers string) error {
	view1 := strings.Fields(numbers)
	view2 := ""
	for _, v := range view1 {
		view2 += v
	}
	view3 := strings.Split(view2, ",")
	view4 := make([][]string, 0)
	for _, v := range view3 {
		view4 = append(view4, strings.Split(v, "-"))
	}
	nums := make([]int, 0)
	for _, v := range view4 {
		if len(v) == 1 {
			n, err := strconv.Atoi(v[0])
			if err != nil {
				panic(err)
			}
			nums = append(nums, n)
		} else if len(v) == 2 {
			n1, err := strconv.Atoi(v[0])
			if err != nil {
				panic(err)
			}
			n2, err := strconv.Atoi(v[1])
			if err != nil {
				panic(err)
			}
			for i := n1; i < n2+1; i++ {
				nums = append(nums, i)
			}
		}
	}
	for _, n := range nums {
		room.Id = uuid.NewString()
		room.NumberRoom = uint16(n)
		if err := p.soburjonRepo.InsertRoom(ctx, room); err != nil {
			return err
		}
	}
	return nil
}

func (p Service) LogIn(ctx context.Context, username, password string) (string, int, error) {

	id, pas, err := p.soburjonRepo.LogInUser(ctx, username, password)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return "", 0, err
	} else if !errors.Is(err, sql.ErrNoRows) {
		if checkPasswordHash(password, pas) {
			return id, 1, nil
		} else {
			return "", 0, PasswordError
		}
	}
	id, pas, err = p.soburjonRepo.LogInOwner(ctx, username, password)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return "", 0, err
	} else if !errors.Is(err, sql.ErrNoRows) {
		if checkPasswordHash(password, pas) {
			return id, 2, nil
		} else {
			return "", 0, PasswordError
		}
	}
	id, pas, err = p.soburjonRepo.LogInAdmin(ctx, username, password)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return "", 0, err
	} else if !errors.Is(err, sql.ErrNoRows) {
		if checkPasswordHash(password, pas) {
			return id, 3, nil
		} else {
			return "", 0, PasswordError
		}
	}
	return "", 0, NoUser
}

func checkPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
