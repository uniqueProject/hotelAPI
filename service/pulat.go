package service

import (
	"context"
	"errors"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"hotel/repository"
	"time"
)

var (
	BusyRoomError        = errors.New("this room is not free")
	NotEnoughMoney       = errors.New("not enough money")
	InvalidPasswordError = errors.New("invalid password")
)

type PulatRepo interface {
	HotelsList(ctx context.Context) ([]repository.HotelShow, error)
	RegisterUser(ctx context.Context, user repository.User) error
	UpdateUser(ctx context.Context, userId string, newUser repository.User) error
	RoomsListOfHotel(ctx context.Context, hotelId string) ([]repository.RoomShow, error)

	GetRoom(ctx context.Context, roomId string) (repository.RoomShow, error)
	GetUser(ctx context.Context, userId string) (repository.User, error)
	UpdateUsersBudget(ctx context.Context, userId string, sum uint16) error
	UpdateHotelsBudget(ctx context.Context, hotelId string, sum uint16) error
	UpdateRoom(ctx context.Context, userId, roomId string, t time.Duration) error

	OrderedRooms(ctx context.Context, userId string) ([]repository.RoomShow, error)
}

func (p Service) ListHotels(ctx context.Context) ([]repository.HotelShow, error) {
	hotelsShow, er := p.pulatRepo.HotelsList(ctx)
	if er != nil {
		return []repository.HotelShow{}, er
	}
	return hotelsShow, nil
}

func (p Service) UserRegister(ctx context.Context, user repository.User) (string, error) {
	user.Id = uuid.NewString()
	if er := p.pulatRepo.RegisterUser(ctx, user); er != nil {
		return "", er
	}
	return user.Id, nil
}

func (p Service) UserUpdate(ctx context.Context, userId string, newUser repository.User) error {
	newUser.Id = userId
	var er error
	if !PasswordCheck(newUser.Password) {
		return InvalidPasswordError
	}
	newUser.Password, er = HashPassword(newUser.Password)
	if er != nil {
		return er
	}
	if er := p.pulatRepo.UpdateUser(ctx, userId, newUser); er != nil {
		return er
	}
	return nil
}

func (p Service) ListRoomsOfHotel(ctx context.Context, hotelId string) ([]repository.RoomShow, error) {
	rooms, er := p.pulatRepo.RoomsListOfHotel(ctx, hotelId)
	if er != nil {
		return []repository.RoomShow{}, er
	}
	return rooms, nil
}

func (p Service) BookingRoom(ctx context.Context, hotelId, userId, roomId string, days int) error {
	room, er := p.pulatRepo.GetRoom(ctx, roomId)
	if er != nil {
		return er
	}
	user, er := p.pulatRepo.GetUser(ctx, userId)
	if er != nil {
		return er
	}
	if room.Status && user.Budget >= uint32(room.Price*uint16(days)) {
		if er := p.pulatRepo.UpdateUsersBudget(ctx, userId, room.Price*uint16(days)); er != nil {
			return er
		}
		if er := p.pulatRepo.UpdateHotelsBudget(ctx, hotelId, room.Price*uint16(days)); er != nil {
			return er
		}
		t := time.Duration(time.Now().Hour() * 24 * days)
		if er := p.pulatRepo.UpdateRoom(ctx, userId, roomId, t); er != nil {
			return er
		}
		return nil
	} else if room.Status {
		return BusyRoomError
	} else if room.Status && user.Budget < uint32(room.Price*uint16(days)) {
		return NotEnoughMoney
	}
	return nil
}

func (p Service) MyOrders(ctx context.Context, userId string) ([]repository.RoomShow, error) {
	rooms, er := p.pulatRepo.OrderedRooms(ctx, userId)
	if er != nil {
		return []repository.RoomShow{}, er
	}
	return rooms, nil
}

func PasswordCheck(password string) bool {
	s, f := 0, []int{0, 0, 0}
	if len(password) > 6 {
		for _, v := range password {
			if v == 32 {
				s = 1
			} else if v >= 65 && v <= 90 {
				f[0] = 1
			} else if v >= 97 && v <= 122 {
				f[1] = 2
			} else if v >= 48 && v <= 57 {
				f[2] = 3
			}
		}
		if s == 0 && f[0]+f[1]+f[2] == 6 {
			return true
		}
		return false
	}
	return false
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 5)
	return string(bytes), err
}
