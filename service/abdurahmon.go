package service

import (
	"context"
	"hotel/repository"
)

type AbdurahmonRepo interface {
	OwnerRegister(ctx context.Context, owner repository.Owner) (string, error)
	ViewMyHotel(ctx context.Context, hotelId string) ([]repository.Hotel, error)
	MyHotels(ctx context.Context, ownerId string) ([]repository.Hotel, error)
	OwnerUpdate(ctx context.Context, newOwner repository.Owner) error
	DeleteHotel(ctx context.Context, hotelId string) error
}

func (s Service) RegisteringOwner() {

}
