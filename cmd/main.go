package main

import (
	"hotel/config"
	"hotel/connect"
	"hotel/repository"
	"hotel/server"
	"hotel/service"
	"log"
)

func main() {
	cfg := config.Load()
	db, er := connect.Connect(cfg)
	if er != nil {
		log.Fatal("er: ", er)
	}

	repo := repository.New(db)
	_ = service.New(*repo)
	server.NewRouter(cfg, nil)

}
