package server

import "github.com/gin-gonic/gin"

// DO NOT WRITE ANY CODE HERE!

type HandlerRequest interface {
	//Po'latning api lari
	HotelList(c *gin.Context)
	RegisterUser(c *gin.Context)
	UpdateUser(c *gin.Context)
	HotelsListRooms(c *gin.Context)
	RoomBooking(c *gin.Context)
	OrdersOfUser(c *gin.Context)

	//Abdurahmonni api lari
	RegisterOwnerAPI(e *gin.Context)
	OwnerHotels(e *gin.Context)
	MyHotels(e *gin.Context)
	ReTakeOwner(e *gin.Context)
	RemoveOwner(e *gin.Context)

	//Shahzod okani api lari
	RegisteringAdmin(e *gin.Context)
	UpdatingAdmin(e *gin.Context)
	DeletingOrder(e *gin.Context)
	ListRoomsOfHotelByAdmin(e *gin.Context)
	RoomOrder(e *gin.Context)

	//Soburjonni api lari
}

// post get put delete metodlari yozilishi kerak bolgan joy:
