package server

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"hotel/repository"
)

type UserJson struct {
	FirstName string `json:"name" bindin:"required"`
	Lastname  string `json:"lastname" bindin:"required"`
	UserName  string `json:"user_name" bindin:"required"`
	Password  string `json:"password" bindin:"required"`
	Email     string `json:"email" bindin:"required"`
	Phone     string `json:"phone" bindin:"required"`
	Budget    uint32 `json:"budget" bindin:"required"`
}

func (s Server) HotelList(c *gin.Context) {
	hotels, er := s.repo.ListHotels(context.Background())
	if er != nil {
		c.JSON(200, gin.H{
			":(": er,
		})
		return
	}
	c.JSON(200, hotels)
}

func (s Server) RegisterUser(c *gin.Context) {
	var request repository.User
	if er := c.ShouldBindJSON(&request); er != nil {
		c.JSON(500, gin.H{
			":(": fmt.Sprintf("enter right json: %v", er),
		})
		return
	}
	userId, er := s.repo.UserRegister(c.Request.Context(), request)
	if er != nil {
		c.JSON(500, gin.H{
			":(": fmt.Sprintf("error while writing to psql: %v", er),
		})
		return
	}
	c.JSON(200, userId)
}

func (s Server) UpdateUser(c *gin.Context) {
	userId := c.Query("id")
	var request repository.User
	if er := c.ShouldBindJSON(&request); er != nil {
		c.JSON(500, gin.H{
			":(": fmt.Sprintf("enter right json: %v", er),
		})
		return
	}
	if er := s.repo.UserUpdate(context.Background(), userId, request); er != nil {
		c.JSON(500, gin.H{
			":(": er,
		})
		return
	}
	c.JSON(200, gin.H{
		":)": "updated!",
	})
}

func (s Server) HotelsListRooms(c *gin.Context) {
	hotelId := c.Query("hotel_id")
	rooms, er := s.repo.ListRoomsOfHotel(context.Background(), hotelId)
	if er != nil {
		c.JSON(500, gin.H{
			":(": er,
		})
		return
	}
	c.JSON(200, rooms)
}

func (s Server) RoomBooking(c *gin.Context) {
	var request repository.BookingR
	if er := c.ShouldBindJSON(&request); er != nil {
		c.JSON(500, gin.H{
			":(": fmt.Sprintf("enter right json: %v", er),
		})
		return
	}
	if er := s.repo.BookingRoom(context.Background(), request.HotelId, request.UserId, request.RoomId, request.Days); er != nil {
		c.JSON(500, gin.H{
			":(": er,
		})
		return
	}
	c.JSON(200, gin.H{
		":)": "You got this room bro",
	})
}

func (s Server) OrdersOfUser(c *gin.Context) {
	rooms, er := s.repo.MyOrders(c.Request.Context())
	if er != nil {
		c.JSON(500, gin.H{
			":(": er,
		})
		return
	}
	c.JSON(200, rooms)
}
