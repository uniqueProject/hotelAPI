package server

import (
	"context"
	"hotel/repository"
)

type Repository interface {
	LogIn(ctx context.Context, username, password string) (string, int, error)
	HotelRegister(ctx context.Context, hotel repository.Hotel) (string, error)
	AddRoom(ctx context.Context, hotelId, numbers string, tip, persons int) error

	OwnerRegister(ctx context.Context, owner repository.Owner) (string, error)
	ViewMyHotel(ctx context.Context, hotelId string) ([]repository.Hotel, error)
	MyHotels(ctx context.Context, ownerId string) ([]repository.Hotel, error)
	OwnerUpdate(ctx context.Context, newOwner repository.Owner) error
	DeleteHotel(ctx context.Context, hotelId string) error

	ListHotels(ctx context.Context) ([]repository.HotelShow, error)
	UserRegister(ctx context.Context, user repository.User) (string, error)
	UserUpdate(ctx context.Context, userId string, newUser repository.User) error
	ListRoomsOfHotel(ctx context.Context, hotelId string) ([]repository.RoomShow, error)
	BookingRoom(ctx context.Context, hotelId, userId, roomId string, days int) error
	MyOrders(ctx context.Context) ([]repository.RoomShow, error)

	AdminRegister(ctx context.Context, admin repository.Admin) (string, error)
	AdminUpdate(ctx context.Context, newAdmin repository.Admin) error
	OrderDelete(ctx context.Context, adminId, roomId string) error
	ListRoomsOfHotelByAdmin(ctx context.Context, adminId string) ([]repository.Room, error)
	OrderRoom(ctx context.Context, adminId, roomId string) error
}
