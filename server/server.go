package server

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"hotel/config"
	_ "hotel/docs"
	"log"
	"net"
)

type Server struct {
	repo Repository
}

// @title           Postgres Hotel API
// @version         1.0
// @description     This is a hotel server.

// NewRouter
// @contact.name   API Support
// @contact.url    http://www.swagger.io/support
// @contact.email  support@swagger.io
func NewRouter(cnf config.Config, repo Repository) {
	_ = Server{
		repo: repo,
	}

	r := gin.Default()

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	if er := r.Run(net.JoinHostPort(cnf.Host, cnf.Port)); er != nil {
		log.Fatal("er: ", er)
	}
}
