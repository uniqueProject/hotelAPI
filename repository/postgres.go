package repository

import (
	"database/sql"
)

type PsqlRepo struct {
	db *sql.DB
}

func New(db *sql.DB) *PsqlRepo {
	return &PsqlRepo{
		db: db,
	}
}
