package repository_test

import (
	"context"
	"fmt"
	"github.com/go-playground/assert/v2"
	"github.com/stretchr/testify/require"
	"hotel/repository"
	"testing"
)

func TestPsqlRepoUser(t *testing.T) {
	t.Run("list of hotels", func(t *testing.T) {
		psql := repository.PsqlRepo{}
		fmt.Println(1)
		hotels, er := psql.HotelsList(context.Background())
		fmt.Println(2)
		require.NoError(t, er)
		fmt.Println(3)
		assert.Equal(t, 1, len(hotels))
	})
}
