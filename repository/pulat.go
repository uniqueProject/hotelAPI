package repository

import (
	"context"
	"time"
)

func (p PsqlRepo) HotelsList(ctx context.Context) ([]HotelShow, error) {
	rows, er := p.db.QueryContext(ctx, `select id, name, star, admin_id, address from hotels`)
	if er != nil {
		return nil, er
	}
	var hotels []HotelShow
	for rows.Next() {
		var hotel HotelShow
		if er := rows.Scan(&hotel.Id, &hotel.Name, hotel.Star, hotel.AdminId, hotel.Address); er != nil {
			return nil, er
		}
		hotels = append(hotels, hotel)
	}
	return hotels, nil
}

func (p PsqlRepo) RegisterUser(ctx context.Context, user User) error {
	if _, er := p.db.ExecContext(ctx, "insert into users values ($1, $2, $3, $4, $5, $6, $7, $8)", user.Id, user.FirstName, user.Lastname, user.UserName, user.Password, user.Email, user.Phone, user.Budget); er != nil {
		return er
	}
	return nil

}

func (p PsqlRepo) UpdateUser(ctx context.Context, userId string, newUser User) error {
	if _, er := p.db.ExecContext(ctx, `update users set first_name = $1, last_name = $2, user_name = $3, password = $4, email = $5, phone = $6, budget = $7 where id = $8`, newUser.FirstName, newUser.Lastname, newUser.UserName, newUser.Password, newUser.Email, newUser.Phone, newUser.Budget, userId); er != nil {
		return er
	}
	return nil
}

func (p PsqlRepo) RoomsListOfHotel(ctx context.Context, hotelId string) ([]RoomShow, error) {
	rows, er := p.db.QueryContext(ctx, `select id, price, hotel_id, room_number, type, status, persons from rooms`)
	if er != nil {
		return nil, er
	}
	var rooms []RoomShow
	for rows.Next() {
		var room = RoomShow{}
		if er := rows.Scan(&room.Id, &room.Price, &room.HotelId, &room.NumberRoom, &room.Type, room.Status, room.Persons); er != nil {
			return nil, er
		}
		rooms = append(rooms, room)
	}
	return rooms, nil
}

func (p PsqlRepo) GetRoom(ctx context.Context, roomId string) (RoomShow, error) {
	row := p.db.QueryRowContext(ctx, `select id, price, hotel_id, room_number, type, status, persons from room where id = $1`, roomId)
	room := RoomShow{}
	if er := row.Scan(&room.Id, &room.Price, &room.HotelId, &room.NumberRoom, &room.Type, &room.Status, &room.Persons); er != nil {
		return RoomShow{}, er
	}
	return room, nil
}

func (p PsqlRepo) GetUser(ctx context.Context, userId string) (User, error) {
	row := p.db.QueryRowContext(ctx, `select * from users where id = $1`, userId)
	user := User{}
	if er := row.Scan(&user.Id, &user.FirstName, &user.Lastname, &user.UserName, &user.Password, &user.Email, &user.Phone, &user.Budget); er != nil {
		return User{}, er
	}
	return user, nil
}

func (p PsqlRepo) UpdateUsersBudget(ctx context.Context, userId string, sum uint16) error {
	_, er := p.db.ExecContext(ctx, `update users set budget = - $1 where id = $2`, sum, userId)
	return er
}

func (p PsqlRepo) UpdateHotelsBudget(ctx context.Context, hotelId string, sum uint16) error {
	_, er := p.db.ExecContext(ctx, `update hotels set budget = budget + $1 where id = $2`, sum, hotelId)
	return er
}

func (p PsqlRepo) UpdateRoom(ctx context.Context, userId, roomId string, t time.Duration) error {
	_, er := p.db.ExecContext(ctx, `update rooms set user_id = $1, expire = $2 where id = $3`, userId, t, roomId)
	return er
}

func (p PsqlRepo) OrderedRooms(ctx context.Context, userId string) ([]RoomShow, error) {
	rows, er := p.db.QueryContext(ctx, `select id, price, hotel_id, room_number, type, status, persons from rooms where user_id = $1`, userId)
	if er != nil {
		return nil, er
	}
	var rooms []RoomShow
	for rows.Next() {
		room := RoomShow{}
		if er := rows.Scan(&room.Id, &room.Price, &room.HotelId, &room.NumberRoom, &room.Type, &room.Status, &room.Persons); er != nil {
			return nil, er
		}
		rooms = append(rooms, room)
	}
	return rooms, nil
}
