package repository

import "context"

func (p PsqlRepo) OwnerRegister(ctx context.Context, owner Owner) (string, error) {
	_, err := p.db.Exec(`insert into owner values($1,$2,$3,$4,$5,$6,$7)`,
		owner.Id,
		owner.FirstName,
		owner.LastName,
		owner.UserName,
		owner.Password,
		owner.Email,
		owner.Phone)
	if err != nil {
		return "", err
	}
	return owner.Id, nil
}
func (p PsqlRepo) ViewMyHotel(ctx context.Context, hotelId string) ([]Hotel, error) {
	var hotels []Hotel

	rows, err := p.db.Query(`select * from hotels where id = $1`, hotelId)
	if err != nil {
		return hotels, err
	}
	for rows.Next() {
		var hotel Hotel
		rows.Scan(&hotel.Id, &hotel.Name,
			&hotel.Star, &hotel.Owner, &hotel.Admin,
			&hotel.Address, &hotel.History, &hotel.Budget)
		hotels = append(hotels, hotel)
	}
	return hotels, nil
}
func (p PsqlRepo) MyHotels(ctx context.Context, ownerId string) ([]Hotel, error) {
	var hotels []Hotel
	rows, err := p.db.Query(`select * from hotels where id = $1`, ownerId)
	if err != nil {
		return hotels, err
	}
	for rows.Next() {
		var hotel Hotel
		rows.Scan(&hotel.Id, &hotel.Name, &hotel.Star,
			&hotel.Owner, &hotel.Admin, &hotel.Address, &hotel.History)
		hotels = append(hotels, hotel)
	}
	return hotels, nil
}
func (p PsqlRepo) OwnerUpdate(ctx context.Context, newOwner Owner) error {
	_, err := p.db.Exec(`update owner set first_name = $1,last_name = $2,username = $3,password = $4,email = $5,phone=$6 where id = $8`,
		newOwner.FirstName, newOwner.LastName, newOwner.UserName, newOwner.Password, newOwner.Email, newOwner.Phone, newOwner.Id)
	if err != nil {
		return err
	}
	return nil
}
func (p PsqlRepo) DeleteHotel(ctx context.Context, hotelId string) error {
	_, err := p.db.Exec(`delete from hotels where id = $1`, hotelId)
	if err != nil {
		return err
	}
	return nil
}
