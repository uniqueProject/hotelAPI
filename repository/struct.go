package repository

import "time"

type Room struct {
	Id         string
	Price      uint16
	HotelId    string
	NumberRoom uint16
	Type       uint8
	Status     bool
	Persons    uint8
	UserId     string
	Expire     time.Time
}

type RoomShow struct {
	Id         string `json:"id" bindin:"required"`
	Price      uint16 `json:"price" bindin:"required"`
	HotelId    string `json:"hotel_id" bindin:"required"`
	NumberRoom uint16 `json:"number_room" bindin:"required"`
	Type       uint8  `json:"type" bindin:"required"`
	Status     bool   `json:"status" bindin:"required"`
	Persons    uint8  `json:"persons" bindin:"required"`
}

type HotelShow struct {
	Id      string `json:"id" bindin:"required"`
	Name    string `json:"name" bindin:"required"`
	Star    uint8  `json:"star" bindin:"required"`
	AdminId string `json:"admin_id" bindin:"required"`
	Address string `json:"address" bindin:"required"`
}

type Hotel struct {
	Id      string
	Name    string
	Star    uint8
	Owner   string
	Admin   string
	Address string
	History string
	Budget  uint
}
type User struct {
	Id        string
	FirstName string `json:"name" bindin:"required"`
	Lastname  string `json:"lastname" bindin:"required"`
	UserName  string `json:"user_name" bindin:"required"`
	Password  string `json:"password" bindin:"required"`
	Email     string `json:"email" bindin:"required"`
	Phone     string `json:"phone" bindin:"required"`
	Budget    uint32 `json:"budget" bindin:"required"`
}

type BookingR struct {
	HotelId string `json:"hotel_id" bindin:"required"`
	UserId  string `json:"user_id" bindin:"required"`
	RoomId  string `json:"room_id" bindin:"required"`
	Days    int    `json:"days" bindin:"required"`
}

type Admin struct {
	Id        string
	FirstName string
	LastName  string
	UserName  string
	Phone     string
	Email     string
	Password  string
}

type Owner struct {
	Id        string
	FirstName string
	LastName  string
	UserName  string
	Phone     string
	Email     string
	Password  string
}
