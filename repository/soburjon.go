package repository

import (
	"context"
)

func (p PsqlRepo) InsertHotel(ctx context.Context, hotel Hotel) error {
	_, err := p.db.ExecContext(ctx, `insert into hotels VALUES($1,$2,$3,$4,$5,$6,$7,$8)`,
		hotel.Id, hotel.Name, hotel.Star, hotel.Owner, hotel.Admin, hotel.Address, hotel.History, hotel.Budget)
	if err != nil {
		return err
	}
	return nil
}

func (p PsqlRepo) InsertRoom(ctx context.Context, room Room) error {
	_, err := p.db.ExecContext(ctx,
		`INSERT INTO rooms (id,price,hotelid,numberroom,type,status,persons) VALUES ($1,$2,$3,$4,$5,$6,$7)`,
		room.Id, room.Price, room.HotelId, room.NumberRoom, room.Type, false, room.Persons)
	if err != nil {
		return err
	}
	return nil
}

func (p PsqlRepo) LogInUser(ctx context.Context, username, password string) (string, string, error) {
	id := ""
	passwordDB := ""
	row1 := p.db.QueryRowContext(ctx, `select id,password from users WHERE username=$1`, username)
	if err := row1.Scan(&id, &passwordDB); err != nil {
		return "", "", err
	}
	return id, passwordDB, nil
}

func (p PsqlRepo) LogInOwner(ctx context.Context, username, password string) (string, string, error) {
	id := ""
	passwordDB := ""
	row1 := p.db.QueryRowContext(ctx, `select id,password from owners WHERE username=$1`, username)
	if err := row1.Scan(&id, &passwordDB); err != nil {
		return "", "", err
	}
	return id, passwordDB, nil
}

func (p PsqlRepo) LogInAdmin(ctx context.Context, username, password string) (string, string, error) {
	id := ""
	passwordDB := ""
	row1 := p.db.QueryRowContext(ctx, `select id,password from admins WHERE username=$1`, username)
	if err := row1.Scan(&id, &passwordDB); err != nil {
		return "", "", err
	}
	return id, passwordDB, nil
}
